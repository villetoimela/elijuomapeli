import React, { useState } from "react"
import PlayMode from "./components/PlayMode";
import StartMenu from "./components/StartMenu";

export default function App() {
  const [mode, setMode] = useState("start")



  return (
    <div>

      {mode === "start" && (
        <StartMenu onStartClick={() => setMode("play")} />
      )}

      {mode === "play" && <PlayMode />}

    </div>
  )
}
