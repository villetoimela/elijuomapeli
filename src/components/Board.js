import React, { useState } from 'react'
import generateBoard from '../utils/generateBoard';

export default function Board({ setIsDrink }) {

  const [board, setBoard] = useState(generateBoard())

  const handleSelect = (index) => {

    if (board[index] === 0) {
      const updatedBoard = [...board];
      updatedBoard[index] = -1;
      setBoard(updatedBoard);
    }
    else {
      setIsDrink(true);
    }

  };

  return (

    <div className="grid grid-cols-4 gap-2 max-w-xl mx-auto">
      {board.map((value, index) => {
        return (
          <div className="w-full h-28 flex justify-center p-1 bg-transparent"
            key={index}
            onClick={() => handleSelect(index)}
          >
            {value >= 0 && <img src="/beer.svg" alt="kalja" />}
          </div>
        );
      })}
    </div>
  );
}
