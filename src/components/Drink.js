import React, { useEffect, useState } from 'react'

export default function Drink({ setIsDrink }) {

    const [animate, setAnimate] = useState(false);

    useEffect(() => {
        setAnimate(true);
    }, []);

    return (
        <div>
            <div className="bg-transparent">
                <img
                    className="h-14 mx-auto m-5"
                    onClick={() => setIsDrink(false)}
                    src="/rotate.svg"
                    alt="uudestaan" />
            </div>
            <div className="h-[50vh] w-full flex justify-center items-center bg-transparent" >

                <img src="/beer.svg" alt="kalja"
                    className="h-24 pl-2 transition-all duration-1000"
                    style={{
                        transform: `scale(${animate ? 3 : 1})`,
                    }} />
            </div>
        </div>

    )
}
