import React from 'react'

export default function StartMenu({ onStartClick }) {
    return (
        <div className="border-black border-8 min-h-screen bg-gradient-to-tr from-pink-300 via-purple-300 to-indigo-400">
            <div className="relative w-full">
                <div className="relative  py-12 z-10 max-w-3xl px-3 mx-auto space-y-5 text-center lg:px-0">
                    <h1 className="text-3xl tracking-wide pb-6 bg-white shadow-2xl border-black border-4 text-gray-900 text-dark md:text-7xl">
                        Tervetuloa <span className="underline underline-offset-8">EliJuomapeliin</span>
                    </h1>
                    <h2 className="text-md pb-12 bg-white border-black border-4 shadow-2xl text-lg text-gray-900">
                        <p className="pb-4">Eikö juhlajuomat meinaa mennä alas?</p>
                        <p className="pb-4">Tuntuuko, että rupeaisi väsyttämään?</p>
                        <p >No nyt on helpotusta tarjolla! Sen pelaaminenkin on niin yksinkertaista, että se onnistunee vaikka kolmannen päivän jatkoilla! </p>
                    </h2>
                    <button className="text-gray-900 border-black border-4 shadow-2xl bg-white rounded w-full py-4 uppercase text-center"
                        onClick={onStartClick}>
                        Aloita tästä
                    </button>
                </div>

            </div>
        </div>
    )
}
