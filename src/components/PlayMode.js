import React, { useState } from 'react'
import Drink from './Drink';
import Board from './Board';

export default function PlayMode() {

    const [isDrink, setIsDrink] = useState();

    return (
        <div className="bg-gradient-to-tr from-pink-300 via-purple-300 to-indigo-400 border-black border-8 min-h-screen w-full flex-col justify-center items-center">
            <div className="text-center shadow-2xl max-w-lg sm:mx-auto mb-10 bg-white border-black border-4 m-3 ">
                <h2 className=" text-lg mb-3">Säännöthän ovat erittäin selkeät:</h2>
                <p className="">Jokainen painaa tuoppia vuorotellen, yhden alta löytyy isompi tuoppi ja sitä painanut juo!</p>
            </div>
            <div className="w-full p-3">

                <div className="">
                    {isDrink ? (
                        <Drink setIsDrink={setIsDrink} />
                    ) : (
                        <Board setIsDrink={setIsDrink} />
                    )}
                </div>
            </div>
        </div>
    )
}
